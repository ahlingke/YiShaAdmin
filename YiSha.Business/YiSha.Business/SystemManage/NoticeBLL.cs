﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.SystemManage;
using YiSha.Model.Param.SystemManage;
using YiSha.Service.SystemManage;

namespace YiSha.Business.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-09 12:53
    /// 描 述：公告业务类
    /// </summary>
    public class NoticeBLL
    {
        private NoticeService noticeService = new NoticeService();

        #region 获取数据
        public async Task<TData<List<NoticeEntity>>> GetList(NoticeListParam param)
        {
            TData<List<NoticeEntity>> obj = new TData<List<NoticeEntity>>();
            obj.Data = await noticeService.GetList(param);
            obj.Total = obj.Data.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<NoticeEntity>>> GetPageList(NoticeListParam param, Pagination pagination)
        {
            TData<List<NoticeEntity>> obj = new TData<List<NoticeEntity>>();
            obj.Data = await noticeService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<NoticeEntity>> GetEntity(long id)
        {
            TData<NoticeEntity> obj = new TData<NoticeEntity>();
            obj.Data = await noticeService.GetEntity(id);
            if (obj.Data != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(NoticeEntity entity)
        {
            TData<string> obj = new TData<string>();
            await noticeService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await noticeService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
