﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.SystemManage;
using YiSha.Model.Param.SystemManage;

namespace YiSha.Service.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-09 12:53
    /// 描 述：公告服务类
    /// </summary>
    public class NoticeService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<NoticeEntity>> GetList(NoticeListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<NoticeEntity>> GetPageList(NoticeListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<NoticeEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<NoticeEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(NoticeEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                await entity.Modify();
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<NoticeEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<NoticeEntity, bool>> ListFilter(NoticeListParam param)
        {
            var expression = LinqExtensions.True<NoticeEntity>();
            if (param != null)
            {
            }
            return expression;
        }
        #endregion
    }
}
