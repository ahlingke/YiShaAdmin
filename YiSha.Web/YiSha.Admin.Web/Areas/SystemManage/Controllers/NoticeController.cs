﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.SystemManage;
using YiSha.Business.SystemManage;
using YiSha.Model.Param.SystemManage;

namespace YiSha.Admin.Web.Areas.SystemManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-09 12:53
    /// 描 述：公告控制器类
    /// </summary>
    [Area("SystemManage")]
    public class NoticeController :  BaseController
    {
        private NoticeBLL noticeBLL = new NoticeBLL();

        #region 视图功能
        [AuthorizeFilter("system:notice:view")]
        public ActionResult NoticeIndex()
        {
            return View();
        }

        public ActionResult NoticeForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("system:notice:search")]
        public async Task<ActionResult> GetListJson(NoticeListParam param)
        {
            TData<List<NoticeEntity>> obj = await noticeBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("system:notice:search")]
        public async Task<ActionResult> GetPageListJson(NoticeListParam param, Pagination pagination)
        {
            TData<List<NoticeEntity>> obj = await noticeBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<NoticeEntity> obj = await noticeBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("system:notice:add,system:notice:edit")]
        public async Task<ActionResult> SaveFormJson(NoticeEntity entity)
        {
            TData<string> obj = await noticeBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("system:notice:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await noticeBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
