﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-09 12:53
    /// 描 述：公告实体类
    /// </summary>
    [Table("SysNotice")]
    public class NoticeEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 开始显示时间
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? SendTime { get; set; }
        /// <summary>
        /// 公告标题
        /// </summary>
        /// <returns></returns>
        public string NoticeTitle { get; set; }
        /// <summary>
        /// 公告标签
        /// </summary>
        /// <returns></returns>
        public string NoticeTag { get; set; }
        /// <summary>
        /// 公告内容
        /// </summary>
        /// <returns></returns>
        public string NoticeContent { get; set; }
        /// <summary>
        /// 公告状态 1正常 0紧急
        /// </summary>
        /// <returns></returns>
        public int? NoticeType { get; set; }
        /// <summary>
        /// 公告状态   1已公开 0未公开
        /// </summary>
        /// <returns></returns>
        public int? NoticeState { get; set; }
        /// <summary>
        /// 城市ID
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? CityId { get; set; }
        /// <summary>
        /// 区县ID
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? CountyId { get; set; }
    }
}
